---
layout: handbook-page-toc
title: Vendor Contract Issue Process - Field Marketing and Events
---

{::options parse_block_html="true" /}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc}

- TOC
{:toc}

## Procurement toolkit
<div class="flex-row" markdown="0" style="height:120px;">
  <a href="/handbook/finance/procurement/vendor-selection-process/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;">Vendor selection process</a>  
  <a href="/handbook/finance/procurement/purchase-request-process/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;">Purchase request process</a>
  <a href="/handbook/finance/procurement/vendor-guidelines/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;"><span style="margin-left: auto; margin-right: auto;">Vendor guidelines</span></a>
</div>

## <i class="fas fa-check-circle" id="biz-tech-icons"></i> Is your request for one of the US or Netherlands entities?
> _GitLab Inc, GitLab Federal LLC, GitLab BV, GitLab IT BV_

If yes, then follow the instructions on the table below:

|   | <$25K | Between $25K - $49K | >$50K |
| ------ | ------ | ------ | ------ |
| Field Marketing & Events<br>**New Vendor** | - Create a [“New Supplier” form](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-request-a-new-supplier) in Coupa | - Create a [“New Supplier” form](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-request-a-new-supplier) in Coupa | - Create a [Procurement Intake Issue](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=vendor_contracts_marketing_events) |
| Field Marketing & Events<br>**Existing Vendor** | - Create a [Coupa Requisition](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) | - Create a [Coupa Requisition](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) | - Create a [Procurement Intake Issue](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=vendor_contracts_marketing_events) |

<div class="panel panel-info">
**Unsure if your vendor is new or existing?**
{: .panel-heading}
<div class="panel-body">

- Check the section [How to Search for a Supplier in Coupa](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-search-for-a-supplier) for a detailed guide.
- If you would prefer to have assistance setting up a new vendor, please ping us in the _#procurement_ channel - indicate you are requesting new vendor setup support and identify the vendor name. A member of the procurement team will respond with next steps.

</div>
</div>


### <i class="far fa-question-circle" id="biz-tech-icons"></i> Learn how to create a Coupa Requisition

- Check the [How to Create a Requisition in Coupa](/handbook/business-technology/enterprise-applications/guides/coupa-guide/#how-to-create-a-requisition) page for a detailed walkthrough guide.

## <i class="fas fa-check-circle" id="biz-tech-icons"></i> Is your request for any other GitLab entity?

> _GitLab GK, GitLab Korea Limited, GitLab UK Limited, GitLab GmbH, GitLab PTY Ltd, GitLab Canada Corp., GitLab France SAS, GitLab Ireland Limited or GitLab Singapore Holding PTE LTD_

As we implement Coupa in a [phased approach](/handbook/finance/procurement/coupa-faq/#does-the-coupa-implementation-impact-all-gitlab-entities), Purchase Requests for all other GitLab entities will continue to use the existing procurement issues. Check the [_Deep Dive on the Field Marketing and Events Vendor Purchase Request issue process_](handbook/finance/procurement/vendor-contract-marketing/#-deep-dive-on-the-field-marketing-and-events-vendor-purchase-request-process) section for a detailed walkthrough guide.


## <i class="fas fa-file-signature" id="biz-tech-icons"></i> Deep Dive on the Field Marketing and Events Vendor Purchase Request issue process
Review the [Prior to Contacting Procurement](https://about.gitlab.com/handbook/finance/procurement/prior-to-contacting-procurement/) page first.

**If your vendor engagement includes the sharing, exchange, processing, or storage of data that is NOT public, do NOT use this template. Instead use the General & Professional Services template.**

Complete Steps 1-2, the Intake Steps, with as much specificity and detail as possible. Consider that many of the approvers are just now learning about this purchase for the first time and have no context beyond what you share here. This is your chance to provide all information as clearly as possible to expedite approvals as quickly as possible!

### <i class="fas fa-file-signature" id="biz-tech-icons"></i> Step 1 | Business Needs Intake
Completion of all fields is necessary for approvals. 
Any missing or incomplete fields will result in a delay to the review and/or approval of your request.

**Name this issue based on the following format - [Vendor Name] - [Name of Event or Tactic] - [Date/Date Range of Event or Tactic]
Attach the UNSIGNED vendor contract here: Vendor Contract**
   * This ensures AP can find the issue later to avoid payment delays.

**Provide vendor name**

**Contract start date**
   * The date the work/event takes place, NOT the date signed.

**Contract end date: End Date**
   * Te date the work/event is completed.

**Vendor's verified signature contact** 
   * This is the main contact. More than likely this is your sales rep.
   * This contact will be used if countersignature is required. 

**Vendor's verified billing contact**
   * This is NOT your sales rep. 
   * This is the contact AP will use to facilitate payment and Tipalti setup. 
   * Include the Vendor Billing Contact Name & Email

**Invoice Approver (For Field Marketing - Country Manager)** 
   * This is the GitLab employee who is responsible for approving the invoice.
   * Optional - Additional POC to be copied on Tipalti approvals (For Field Marketing - FMCs): Additional POC

**Designate Campaign Tag in ISOdate_CampaignShortName format**
   * Should be pulled directly from your budget document

**Is this an in-person event?**
   * In-Person events require additional review.

### <i class="fas fa-file-signature" id="biz-tech-icons"></i> Step 2 | Details
Completion of all fields is necessary for approvals. Any missing or incomplete fields will results in a delay to the review and/or approval of your request.

**Total cost in the currency the invoice is to be paid in**
   * If the contract/invoice cost is NOT in USDs, please add the USD cost that is listed in the budget

**Provide overall event cost (listed in the Marketing budget)**

**Provide the expected pipeline ROI**

**Provide the expected MQL: MQL**

**For Field Marketing: If your cost per MQL is more than $500, then please document the business reason why this investment needs to be made.**

**Line item in the Marketing budget**
   * Include link to the budget line item

**If we sponsored last year, report back ROI results via our linear attribution model from Periscope.**

**Vendor Name, and URL**

**Vendor contact**
   * The vendor main point of contact for this purchase. More than likely this is the sales rep you are working with. 
   * We will use this name and email to execute the contract signature. If this field is not completed with the correct name and email, your contract signature will be delayed.

**Vendor billing contact**
   * This is NOT the same as the sales rep - even if the rep tells you so :)
   * This is the actual accounts payable contact who our AP team will be coordinating invoice payment with

**Unsigned contract/quotes**
   * This is where we will look to review the proposal if available.
   * If you have a renewal or a true up, attach the previous order form here so we can reference terms and pricing. Failure to attach will result in review and approval delays.

**Total Contract Value**
   * Identify the estimated contract value. If it is a multi-year term, identify both the annual and total value. 
      * Note we do not do multi-year agreements except in specific circumstances. 
      * If you believe this is a specific circumstance, identify that here, and add both our CFO and CEO as approvers in Step 4.
   * If this is a new vendor and your spend is greater than $100K, check this box and attach the competitive quote from 1-2 other suppliers.
      * For questions on vendor selection process see [Prior to Contacting Procurement](https://about.gitlab.com/handbook/finance/procurement/prior-to-contacting-procurement/)
   * If this is a new vendor and your spend is greater than $250K, check this box and link the RFP issue here.
      * For questions on vendor selection process see [Prior to Contacting Procurement](https://about.gitlab.com/handbook/finance/procurement/prior-to-contacting-procurement/)


### <i class="fas fa-file-signature" id="biz-tech-icons"></i> Step 3 | Approvals
Tag your functional leader(s) according to the [Authorization Matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/#authorization-matrix).
   * Reference the Operating Expenses and Vendor Contracts table.
For Field Marketing contracts, choose one of the approval options and delete the remaining options that do not apply:

If you are in Field Marketing, please add the below Field Marketing Approval Matrix. Select the appropriate approvals based on your contract threshold and copy and paste into the approval section of the procurement template.

### Field Marketing Approval Matrix
Contracts between $0 & $9,999 USD require approval checkoff from:

- [ ] `@ mention DRI`
- [ ] `@ mention the Field Marketing Country Manager`

Contracts between $10,000 - $24,999 USD require approval checkoff from:

- [ ] `@ mention DRI`
- [ ] `@ mention the Field Marketing Country Manager` - once the FM Country Manager has signed off, he/she will add the Global Director of Field Marketing
- [ ] `Global Director of Field Marketing`

Contracts between $25,000 - $49,999 USD require approval checkoff from:

- [ ] `@ mention DRI`
- [ ] `@ mention the Field Marketing Country Manager` - once the FM Country Manager has signed off, he/she will add the Global Director of Field Marketing
- [ ] `Global Director of Field Marketing` - once the Global Director of Field Marketing has signed off, he/she will add the VP of Revenue Marketing
- [ ] `VP of Revenue Marketing`

Contracts between $50,000 - $99,999 USD require approval checkoff from:

- [ ] `@ mention DRI`
- [ ] `@ mention the Field Marketing Country Manager` - once the FM Country Manager has signed off, he/she will add the Global Director of Field Marketing
- [ ] `Global Director of Field Marketing` - once the Global Director of Field Marketing has signed off, he/she will add the VP of Revenue Marketing
- [ ] `VP of Revenue Marketing` - once the VP of Rev Marketing has signed off he/she will tag the CMO
- [ ] `@ mention the CMO`

Tag your finance business partner under Budget Approval.

Scroll to the bottom of the issue and select a due date, if relative, and we will do our best to accomodate. All reviewers and approvers are notified so long as you do not edit below the section that says DO NOT EDIT BELOW.

**SUBMIT YOUR ISSUE!**

Now the review and approval process can begin. 

**A contract can not be signed until each function has reviewed and approved**

Here is a deep dive summary on what each function is reviewing for approval:

**Functional Review and Approval**
1. Please consult the [Authorization Matrix](/handbook/finance/authorization-matrix/) or the Field Marketing Matrix if appropriate to determine who must sign off on Functional Approval and Financial Approval.  
1. The approval from the functional leader is confirming they approve this specific spend to this specific vendor, and that it aligns with their specific business goals as a functional leader.

**Budget Review and Approval**
1. Finance is responsible for confirming the purchase is in budget.
1. Finance is responsible for reviewing and validating the Business Justification. If questions or concerns on the Business Justification, fp&a partner will engage VP, Finance.
1. If the contract exceeds $100K, the CFO will be added as an additional approver.
1. Finance validates the Department, and GL Account.
1. Finance validates the correct entity:
   * PEO's, Contractors should be engaged with GitLab IT BV
   * Contract to be engaged locally when there is a GitLab entity available (e.g. Netherlands with BV, UK with Ltd et cetera)
   * If there is no GitLab entity available in the country of a vendor use GitLab Inc

**Procurement Review and Approval**
1. Before approving issues, the procurement team verifies:
   * Correct purchase request issue type was used.
   * Correct individuals were tagged and have approved according to the [Authorization Matrix](/handbook/finance/authorization-matrix/)?
   * Vendor Selection and Negotiation process was followed:
      * If the contract is <$100K, procurement will negotiate non-software agreements on an as-needed basis if opportunity for savings is identified.
      * If the contract is >$100K, more than one bid is required and the contract must be negotiated
      * If the contract is >$250K, an RFP is required and the contract must be negotiated
   * Contract is Reviewed
      * Procurment confirms dollar amounts match, confirm currency, confirm entities match in quote, and general scope is addressed.
      * If entities do not match, procurement will tag the issue creator and budget approver to confirm which entity should be used. 
      * If negotiated, procurement owns all commercial terms in the contract review process.
2. Procurement Approval Authority
   * Procurement Ops Analyst can approve field marketing and events purchase requests up to $100K 
   * Field marketing and events purchase requests >$100K will be approved by the Sr. Mgr Procurement
   * If it is an on-site event or may involve data that is not publically available, the Sr. Mgr of Procurement must approve.
3. Approval Process
   * In the event procurement approves the purchase request issue before the other approvers to avoid being a bottleneck, procurement will approve before all parties.
   * If this happens, procurement will note our approval as pending the remaining approvers. 
   * At this point it is the responsibility of the issue owner as the DRI to follow the remaining process and secure remaining approvals BEFORE obtaining contract signature.

**Legal Review and Approval**
1. Legal is responsible for reviewing vendor contracts and will adhere to the legal playbook.
1. A contract cannot be signed until it has been **stamped** with approval by the legal team. Once the legal team approves the contract, legal will upload the contract with the approval stamp. 

### <i class="fas fa-file-signature" id="biz-tech-icons"></i> Step 5 | Contract Signature
1. Do not send the contract to the authorized signatory until all approvals are received. Doing so can put GitLab in a direct financial and/or legal risk which could need to be escalated.
1. Once all approvals in the issue are received, send the contract to the authorized signatory:
     - Upload the contract with the legal stamp to HelloSign
     - If a legal stamp is not included in the issue, please request clarification from legal and/or procurement by tagging them in the contract issue or asking for clarification in the #procurement slack channel.
     - In the description field in HelloSign, paste the link to the vendor contract issue request to avoid delays in signature.
     - Enter the signatory's name and email in HelloSign
 1. Once the contract is signed by GitLab, send the contract to your vendor (if not already signed by the vendor) through HelloSign.

- Note: If your vendor has a signature tool they would like to use and all approvals have been received in the contract issue, request that the vendor send the final contract version with the legal stamp for signature and also include the issue link. This will avoid delays in signature.

### <i class="fas fa-file-signature" id="biz-tech-icons"></i> Step 6 | Upload Contract
 1. After the contract is signed by both parties, upload the fully executed contract to ContractWorks. You will need to upload the fully signed pdf into the folder labeled **01. To Be Standardized**, which in under the parent folder **01. Uncategorized**. Legal will then organize the contracts using their [instructions and best practices](/handbook/legal/vendor-contract-filing-process)
      - If you need access to ContractWorks, please process an access request [here](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Single+Person+Access+Request) or tag procurement at @gitlab-com/Finance-Division/procurement-team in the issue and we will upload the contract on your behalf.

### <i class="fas fa-file-signature" id="biz-tech-icons"></i> Step 7 | Create Professional Services Access Request (Optional)
   * Similar to our Access Request process for team members, we have an access request process for consultants or professional services providers. 
   * If the vendor requires access to systems to complete work, the vendor manager (ie. the GitLab team member who will manage the relationship with the temporary service provider, generally a people manager) is responsible for creation of a Vendor Access Request and Orientation issue. 

   * These issues aren't created in the same location as access requests for employees so find the link below so use this [access request template](https://gitlab.com/gitlab-com/contingent-workers/lifecycle/-/issues/new?issuable_template=access-request) and assign it to yourself and the relevant provisioner(s) for the tools that the professional services provider requires access to. 
   * In the AR, include [**only systems that are necessary**](/handbook/engineering/security/access-management-policy.html#access-management) to the work that the vendor will be performing. 

   * If the professional services provider wants support through the set up of the most common tools used at GitLab, create an [orientation issue](https://gitlab.com/gitlab-com/contingent-workers/lifecycle/-/issues/new?issuable_template=orientation-issue). Assign to yourself and the professional services provider if they have a GitLab account with the required access.

### <i class="fas fa-file-signature" id="biz-tech-icons"></i> Step 8 | Accounts Payable
1. Vendors will be required to create an account within Tipalti in order to receive payment
1. For complete details on how to obtain payment, please visit Accounting's [Procure to Pay](/handbook/finance/accounting/#procure-to-pay) page.
1. If your annual contract value is equal to or greater than $100K, a Purchase Order must be created to pay the vendor. See Creating a Purchase Order for steps to do so.

If you have additional questions, please ask in #procurement slack channel. Or attend Purchasing Office Hours, available in the GitLab Team Calendar.

## How do I request approval for a purchase?

After the Vendor Contract Request issue is opened or the requisition is created in Coupa, Procurement will negotiate the best pricing up front to keep our ongoing costs to a minimum across our long-term realtionships with vendors. We also continue to evaluate supplier pricing at the time of renewal to minimize our ongoing costs across our long-term relationships with vendors.
> Create the requisition **BEFORE** agreeing to business terms and/or pricing.

{::options parse_block_html="false" /}
