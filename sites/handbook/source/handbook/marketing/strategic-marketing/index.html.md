---
layout: handbook-page-toc
title: Portfolio Marketing
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Portfolio marketing Teams

1. [Product marketing team](/handbook/marketing/strategic-marketing/pmmteam/)
  - [Competitive intelligence](/handbook/marketing/strategic-marketing/competitive/)
1. [Technical marketing team](/handbook/marketing/strategic-marketing/technical-marketing/)
1. [Market insights team](/handbook/marketing/strategic-marketing/mrnci/)
  - [Analyst relations (AR)](/handbook/marketing/strategic-marketing/analyst-relations/)
  - [Customer reference program](/handbook/marketing/strategic-marketing/customer-reference-program/)

### What does Portfolio Marketing do?

Portfolio Marketing (formerly known as "Strategic Marketing") is GitLab's interface to the market. The market is made up of customers, analysts, press, thought leaders, competitors, etc. Portfolio Marketing enables other GitLab teams such as Sales, Marketing, and Channel with narrative, positioning, messaging, and go-to-market strategy to go **outbound** to the market. Portfolio Marketing does market research to gather customer knowledge, analyst views, market landscapes, and competitor intelligence providing marketing insights **inbound** to the rest of GitLab.

#### Portfolio Marketing Inbound

```mermaid
graph LR;
  id1 --> id2
  id2 --> id3
  subgraph "The Market"
    id1[Customers <br>Users <br>Community <br>Press <br>Analysts <br>Thought Leaders <br>Competitors]
  end
  subgraph "Portfolio Marketing"
    id2[Market Research]
  end
  subgraph "GitLab"
    id3[Sales & Marketing Enablement <br>Product Roadmap]
  end
```

#### Portfolio Marketing Outbound

```mermaid
graph LR;
  id1 --> id3
  id2 --> id3
  id3 --> id4
  id4 --> id5

  subgraph "Portfolio Marketing"
    id1[Market Research]
    id3[Go-to-market Assets]
  end
  subgraph "GitLab R&D"
    id2[Product]
  end
  subgraph "GitLab GTM"
    id4[Sales <br>Marketing]
  end
  subgraph "The Market"
    id5[Customers <br>Users <br>Community <br>Press <br>Analysts <br>Thought Leaders <br>Competitors]
  end
```

### Portfolio Marketing Acroyms

Portfolio Marketing, Product Marketing, and Product Managment all have different acroyms so that it's clear which one you are refering to.

- **PFM**: **Portfolio Marketing**. The Entire PFM Group including Product Marketing, Technical Marketing, and Market Inisghts.
- **PMM**: Used to refer to **Product Marketing Management** (as a practice or the entire Product Marketing tream) or specifically to a **Product Marketing Manager**. Examples:
    - Product Marketing Management: "Let's collaborate with PMM since they are customer and market SMEs"
    - Product Marketing Manager: "Parker is the PMM on my cross-functional team."
- **PM**: [Product Mangement](https://about.gitlab.com/handbook/product/) or [Product Manager](https://about.gitlab.com/handbook/product/product-manager-role/). Don't use PM to refer to Portfolio Marketing or Product Marketing, use PM exclusively for the Product team.
- **TMM**: Used to refer to **Technical Marketing Management** (as a practice or the entire Technical Marketing tream) or specifically to a **Technical Marketing Manager**. Use TMM. Don't use TM (which useually stand for "Trademark".)

### Metrics
[Portfolio Marketing Content Analytics](/handbook/marketing/strategic-marketing/metrics/)

### Solutions Go To Market

[Solutions GTM Page](/handbook/marketing/strategic-marketing/usecase-gtm/)

### Sales and partner enablement

Portfolio Marketing team members serve as subject matter experts and conduct sales trainings scheduled by the [Sales Training team](/handbook/sales/training/). For info go to the [Sales Enablement page](/handbook/sales/training/sales-enablement-sessions/).

#### Speaker Abstracts    
  To encourage reuse and collaboration, we have a [shared folder of past abstracts](https://drive.google.com/drive/folders/1ODXxqd4xpy8WodtKcYEhiuvzuQSOR_Gg) for different speaking events.  

### Requesting Portfolio Marketing help

All Portfolio Marketing Work is tracked and managed as issues in the Strategic Marketing Project. If you need support from the team, the simple process below will enable us to support you.

1. [Open an SM Support Request Issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/new?issuable_template=A-SM-Support-Request) this link will the _A-SM-Support-Request_ template. PLEASE fill in what you know.
1. Be sure to @mention a specific team member who you are requesting help from. If you don't @mention a team member, you're request may not be processed. Ping `@williamchia` ,`@traci`, and `@davistye` if you don't know who to tag.
1. If you need more immediate attention please send a message with a link to the issue you created in the `#product-marketing` slack channel. Add an `@reply` to the PMM responsible or you can ping the team with `@pmm-team`.

#### Planning and Reporting Resources  

  - [Portfolio Marketing group conversation slides](https://drive.google.com/drive/folders/1fCEAj1HCegJOJE_haBqxcy2NYm0DS1FO)   
  - [Portfolio Marketing FY 2020 Vision](https://docs.google.com/presentation/d/1sbpBNy5OpO0QGvkAeobNyyIcEjTRGIkyApKeC1Oa8xY/edit#slide=id.g4a712342f9_0_852)
  - [CMO Triage Board](https://gitlab.com/groups/gitlab-com/-/boards/1160244?label_name[]=CMO%20Staff%20Triage)
  - [Portfolio Marketing Staff Triage Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1237365?label_name[]=sm_request)
  - [Technical Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/926375?&label_name[]=tech-pmm)
  - [Partner Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/general/-/boards/814970)
  - [Customer Reference Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?&label_name[]=Customer%20Reference%20Program)
  - [Case Studies Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/918204?&label_name[]=Case%20study)
  - [Portfolio Marketing Staff Triage Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1237365?label_name[]=sm_request)
  - [Sales Enablement Board](https://gitlab.com/gitlab-com/marketing/general/boards/465497?=&label_name[]=Sales%20Enablement)
  - [SDR Coaching Board](https://gitlab.com/gitlab-com/marketing/general/boards/772948?label_name[]=XDR-Coaching)
  - [PMM Budget Planning](https://docs.google.com/spreadsheets/d/1_MN8K9ixdgOp32DKiGPjT-208pr8s-rNKKuuYHsqCdI/edit#gid=1423447843)
  - [PMM Hiring Planning](https://docs.google.com/spreadsheets/d/12mNijMwA8hIG5h3zV5JJ0oxsVh6xzk6-si0eT7L9mvM/edit#gid=1301737184)
  - [Portfolio Marketing Event and Travel Priorities](/handbook/marketing/strategic-marketing/events_travel/)

#### Productivity

  - [Getting Started - GitLab 101 - No Tissues for Issues](/handbook/marketing/strategic-marketing/getting-started/101/)  
  - [Getting Started - GitLab 102 - Working Remotely](/handbook/marketing/strategic-marketing/getting-started/102/)
  - [Getting Started - GitLab 103 - Maintaining common slides across decks](/handbook/marketing/strategic-marketing/getting-started/103/)
  - [Getting Started - GitLab 104 - Building Project Issue Templates](/handbook/marketing/strategic-marketing/getting-started/104/)
  - [Getting Started - GitLab 105 - Label Triage Bot - Automatic Hygiene](/handbook/marketing/strategic-marketing/getting-started/105/)
  - [Getting Started - Strategic Marketing Project Management Overview](/handbook/marketing/strategic-marketing/getting-started/sm-project-management/)

  - [Getting Started - How to Iterate](https://docs.google.com/presentation/d/14wjQoP8mXre-VWyX_e6F4Tp4D7kwvIetHHXEzheuqFM/edit#slide=id.g810dda6128_4_329)
  - [Markdown style guide for about.gitlab.com](/handbook/markdown-guide/#markdown-style-guide-for-aboutgitlabcom)
  - [Searching the GitLab Website Like a Pro](/handbook/tools-and-tips/searching/)
  - [Other frequently used sales resources](/handbook/marketing/strategic-marketing/sales-resources/)  
  - [QBR Support Process](/handbook/marketing/strategic-marketing/qbr/)

### Collatoral

#### Customer facing presentations

Marketing decks linked on this page are the latest approved decks from Portfolio Marketing that should be always be in a state that is ready to present. As such there should never be comments or WIP slides in a marketing deck. If you copy the deck to customize it please give it a relevant title, for example include the name of the customer and an ISO date.

- [Company pitch deck](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/)
The Pitch Deck contains the GitLab narrative.
- [Customer value deck](https://docs.google.com/presentation/d/1SHSmrEs0vE08iqse9ZhEfOQF1UWiAfpWodIE6_fFFLg/edit?usp=sharing)
The Customer deck contains the GitLab value driver narrative and supports a discussion about GitLab differentiators.
- [GitLab security capabilities deck](https://docs.google.com/presentation/d/1WHTyUDOMuSVK9uK7hhSIQ_JbeUbo7k5AW3D6WwBReOg/edit?usp=sharing)
This deck introduces GitLab's position and capabilities around security. It covers why better security is needed now and how GitLab provides that better security in a more effective manner than traditional tools. This deck should be used when talking to prospects who are asking about how GitLab can help them better secure their software via GitLab Ultimate.

To request updates to these decks see [requesting help from the strategic marketing department](##requesting-portfolio-marketing-help)

#### Key Demos

##### Videos

- [GitLab in 3 minutes](https://youtu.be/Jve98tlZ394) (~3 mins)
- [Benefits of a Single Application](https://youtu.be/MNxkyLrA5Aw) (~2.5 minutes)
- [GitLab Overview](https://youtu.be/7q9Y1Cv-ib0) (~12 mins)
- [GitLab for Remote Teams](https://youtu.be/qCDAioq3eis) (~9 mins)

##### Click through Demos

- [GitLab Planning to Monitoring](https://tech-marketing.gitlab.io/static-demos/ptm-v14.html)
- [GitLab Secure Capabilities](https://docs.google.com/presentation/d/1fdTmdepdaq03OSfcA3pYduDxDnQEyvY4ARPqXEX8KrY/edit#slide=id.g2823c3f9ca_0_9)
- [GitLab Agile Project Management](https://docs.google.com/presentation/d/13Zj83pjpwyq3s4T2fPSTuKO8NwqdCdn827GB7S-3hW8/edit?usp=sharing)
- [GitOps with GitLab](https://docs.google.com/presentation/d/e/2PACX-1vQTBQAHx7zyNd4o3YIyKmFEsRJl8-BCdd2g6MdCKuJuFab_HNea_HYK7HDSzd3macx6LnVtYwIlCxV7/embed?start=false&loop=false&delayms=3000)

#### Print collateral

The following list of print collateral is maintained for use at trade shows, prospect and customer visits. The GitLab marketing pages should be downloaded as PDF documents if a print copy for any marketing purposes is needed.

- [GitLab DataSheet](/images/press/gitlab-data-sheet.pdf)
- [GitLab Federal Capabilities One-pager](/images/press/gitlab-capabilities-statement.pdf)
- [How GitLab is Enterprise Class](/solutions/enterprise-class)
- [Reduce cycle time whitepaper](/resources/downloads/201906-whitepaper-reduce-cycle-time.pdf)
- [Speed to mission whitepaper](/resources/downloads/201906-whitepaper-speed-to-mission.pdf)
