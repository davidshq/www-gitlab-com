---
layout: handbook-page-toc
title: "Learn How to Use Chorus.ai"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Confused about how to use Chorus? Check out the links below for more information on how to get the most out of this important sales tool.

## How to Remove Chorus.ai From a Meeting

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.loom.com/share/a6513ac235ae4eb9acaaeb167d7583ce" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- blank line -->

## Chorus.ai Basics

- [How to Remove Chorus From a Meeting](https://www.loom.com/share/a6513ac235ae4eb9acaaeb167d7583ce) 
- [Getting Started with Chorus](https://docs.chorus.ai/hc/en-us/sections/115002365608-Getting-Started-with-Chorus)
- [Chorus FAQs](https://docs.chorus.ai/hc/en-us/sections/115002365588-FAQs)
- [Chorus Basics for SDRs and Reps](https://docs.chorus.ai/hc/en-us/sections/360003251593-Chorus-Basics-for-SDRs-BDRs-and-Reps)
- [Chorus Basics for Managers & Sales Enablement](https://docs.chorus.ai/hc/en-us/sections/115002370787-Chorus-Basics-for-Managers-Sales-Enablement)

## Chorus.ai Advanced Usage

- [Chorus Integrations](https://docs.chorus.ai/hc/en-us/sections/115002215568-Integrations)
- [Compliance](https://docs.chorus.ai/hc/en-us/sections/360001251353-Compliance)
