---
layout: markdown_page
title: DIY DevOps
---

You can build your own "Do It Yourself" DevOps solution for your team, not unlike building a car from parts at the auto store, or building your own computer from scratch.  To Do It Yourself, you would need to spend effort in evaluating multiple different solution for every single usecase, spend budget in procuring multiple different solutions, building and maintain expertise to build this DIY toolchain and maintain its various integrations, OR you could benefit from working with a single platform that solves most, if not all, problems you are looking to solve with a DevOps solution. There are many reasons why there is value in working with a DevOps Platform, rather than the DIY approach.

## Operational Efficiencies

DIY DevOps gives a fragmented DevOps experience - significantly increasing overheads in the development lifecycle, frequent handoffs and developer frustration.

|								|  DIY DevOps		|	GitLab - an open core DevOps platform  |
| --- | ---| --- |
|  Developer Experience		|  Fragmented across multiple tools | Coherent and consistent throughout GUI and CLI command semantics |
|  Developer Efficiency  |  Time wasted switching context between tools  |  Developers are faster because they can focus on the work, not the tools.  |
|  Operational Cost  | Must manage and maintain the tools, tool integrations, licenses, upgrades  |  Lower cost, with just one platform to manage |
|  Focus | Dev focused. | Dev, Ops and Security focused  |
|  Reporting/Visibility  | Silos you have to integrate to access data  |  Single Data store makes it easy to extract out of the box visibility  |
|  Efficiency and Bottlenecks | Work can get lost between tools. |  Visibility with everyone on the same page  |

## Time to Market

DIY DevOps takes longer to setup, onboard and maintain - continuing to maintain the silos which you originally set out to fix - leading to longer time to market and lower quality. 

|								|  DIY DevOps		|	GitLab - an open core DevOps platform  |
| --- | ---| --- |
|  Developer On-boarding  | Must learn multiple tools and interfaces. | Industry standard platform - fast on-boarding. Up to date docs and a constellation of community-driven resources: meetups, courses, tutorials, etc. |
|  Project Start up. |  Requires updates and configuration in multiple tools. |  Just create project in one place and start coding  |
|  DevOps tool innovation 		| Limited to your internal team or vendor  |  Community fuelled innovation with thousands of contributors- Ability to influence roadmap and direct stakeholders as well as visibility through out maturity process. |
|  Tool Integrations  |  Custom integrations requiring ongoing maintenance & management. |  No need for integrations - Single application  |
|  New Versions  | May break existing siloed tools. |  Just works. |
|  Collaboration  |  Separate tools reinforce the silos, need to manually share data across tools and use a separate tool for collaboration  |  Collaboration built into the platform with all relevant context - bridging organizational silos. |

## Security and Compliance

Security is often an afterthought and exception based in DIY DevOps - continuing to be performed in silos and after the development process - resulting in little or no time for fixing security issues before deploying to production. Security is integrated into the process in a DevOps platform - making it a norm rather than an exception.

|								|  DIY DevOps		|	GitLab - an open core DevOps platform  |
| --- | ---| --- |
|  Credentials. | Passed from tool to tool to tool. |  Access to one tool |
|  Access Controls & Permissions |  More risk as you must configure & manage access on multiple tools  |  Manage access in one place and reduce risk |
|  Compliance/Auditing |  Auditor nightmare to extract and reconcile from multiple tools |  Compliance and audits streamlined with data in one place |
|  Security  |  A separate silo, often isolated and performed too late in the development cycle | Security scanning built into the lifecycle. |
