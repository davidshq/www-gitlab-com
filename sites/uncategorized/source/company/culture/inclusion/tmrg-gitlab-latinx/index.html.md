---
layout: markdown_page
title: "TMRG - Latinx"
description: "An overview of our TMRG Latinx"
canonical_path: "/company/culture/inclusion/tmrg-gitlab-latinx/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction
Bienvenidos, bem-vinda and welcome to the Latinx space! We are a new TMRG and very much a WIP. Join us in our slack channel #latinx to be notified of our launch call and feel free to check out what we've been up to so far below.

[Latinx TMRG Overview Slide Deck (WIP)](https://docs.google.com/presentation/d/1eQIv4Km0bd9ciuJ-hjmuPSRo1TZxH1f7FcUfau7xQB4/edit#slide=id.gb5fc211329_1_0)   
[Latinx Project in GitLab](https://gitlab.com/gitlab-com/latinx-tmrg)  
Slack Channel: #latinx  
Google Group: latinxtmrg@gitlab.com

**Purpose of the TMRG**  
The GitLab's Latinxs TMRG provides a safe space for every GitLab Team member within GitLab to raise awareness and celebrate Latin American culture, and to understand how the diversity of this region can be leveraged to contribute to GitLab's strategic objectives. This group is not exclusionary and is open to anyone who is a GitLab employee and interested in learning more about Latin culture.

## Mission

To promote advancement, cultural awareness, talent recruitment, retention, and professional development for Latinx individuals by giving a voice to the diverse backgrounds of the Latinx community in order to ensure the GitLab messaging continues to reflect its core values of Diversity, Inclusion & Belonging.



## Leads
* [Pilar Mejia](https://gitlab.com/pmejia)
* [Hugo Azevedo](https://gitlab.com/hugoazevedo) - Co Lead 
* [Romer Gonzalez](https://gitlab.com/romerg) - Co Lead
* [Chris Cruz](https://gitlab.com/chriscruz) - Co Lead

## Executive Sponsors
Looking for nominations!

## How To Get Involved
* [Join our Google Group here](https://groups.google.com/a/gitlab.com/g/latinxtmrg)

## Upcoming Events 
* [Join us for our Launch Call March 4th](https://calendar.google.com/calendar/u/0/r/eventedit/MXE5MGZpb3JuZzRwaHEzcnBkbWZjcjNrcnFfMjAyMTAzMDRUMTkwMDAwWiBwbWVqaWFAZ2l0bGFiLmNvbQ?tab=mc)

## Community Outreach 
Our Latinx members are proud to be contributing externally to the following charitable organizations dedicated to supporting the Latinx community

* [Techqueria](https://techqueria.org/) - Techqueria is a nonprofit that serves the largest community of Latinx professionals in tech.
* [Latinitas](https://latinitasmagazine.org/) - Empowering all girls to innovate through media and technology
* [Latinas in Tech](https://www.latinasintech.org/) - We are a non-profit organization with the aim to connect, support, and empower Latina women working in tech. We work hand in hand with top technology companies to create safe spaces for learning, mentorship and recruitment..

## Latinx Member Development
Learn more about the Latinx community

* [Why Latinx?](https://www.merriam-webster.com/words-at-play/word-history-latinx) 
* [LTX Fest On-Demand](https://ltxfest.com/ltx-on-demand/) - The largest gathering of Latinx folks in tech, advocacy, and entrepreneurship.
* Contribute to GitLab by [translating GitLab](https://translate.gitlab.com/) the product to promote inclusivity.
* [LinkedIn Learning - Leadership in Tech](https://www.linkedin.com/learning/leadership-in-tech/) - Erica Lockheimer successfully rose through the ranks at LinkedIn to become a VP of engineering. In this course, she interviews other leaders—all of whom inspired her own professional journey—about what it takes to carve out a lasting career in the lively, challenging world of tech.

